const GET_PRODUCTS = "products/GET_PRODUCTS"
const FAV_PRODUCTS = "products/FAV_PRODUCTS"
const ADD_TO_CART = "products/ADD_TO_CART"
const DELETE_FROM_CART = "products/DELETE_FROM_CART"
const REMOVE_ALL = "products/REMOVE_ALL"

const getProducts = (products) => ({ type: GET_PRODUCTS, payload: products })

export const getAllProducts = () => async (dispatch) => {
  const response = await fetch('db.json');

  if (response.ok) {
    const products = await response.json();
    const updatedProducts = checkInFavAndInCart(products.products)
    dispatch(getProducts(updatedProducts));
  }
}

export const addToFav = (id) => ({ type: FAV_PRODUCTS, payload: id })

export const addToCart = (id) => ({ type: ADD_TO_CART, payload: id })

export const removeFromCart = (id) => ({ type: DELETE_FROM_CART, payload: id })

export const removeAllProducts = () => ({ type: REMOVE_ALL})

function checkInFavAndInCart(products) {
  const cart = JSON.parse(localStorage.getItem("cart")) || []
  const fav = JSON.parse(localStorage.getItem("fav")) || []
  const idInCart = cart.map((item) => item.id)
  const updateInCart = products.map(item => {
    if (idInCart.includes(item.id)) {
      const finded=cart.find(el=>el.id===item.id)
      item.cart = true
      return {...item, ...finded}
    }
    return {...item, count:0}
  })
  const updateInFav = updateInCart.map((item) => {
    if (fav.includes(item.id)) {
      item.fav = true

      return item
    }
    return item
  })
  return updateInFav
}

const initialState = {
}

const productsReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case GET_PRODUCTS:
      const newState = payload.reduce((acc, cur) => {
        acc[cur.id] = cur
        return acc
      }, {})
      return newState;
    case REMOVE_ALL:
      const removeState = Object.values(state)
      const updateState = removeState.map(item=>{
        item.cart = false
        return item
      })
      const reduced = updateState.reduce((acc, cur) => {
        acc[cur.id] = cur
        return acc
      }, {})
      return reduced
    case FAV_PRODUCTS:
      const favState = { ...state }
      favState[payload].fav = !favState[payload].fav
      return favState
    case ADD_TO_CART:
      const cartState = { ...state }
      cartState[payload].cart = true
      cartState[payload].count += 1
      return cartState
    case DELETE_FROM_CART:
      const deleteCartState = { ...state }
      deleteCartState[payload].cart = false
      return deleteCartState
    default:
      return state;
  }
}

export default productsReducer;