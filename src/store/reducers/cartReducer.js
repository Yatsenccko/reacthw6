const GET_CART = "cart/GET_CART"
const DELETE_FROM_CART = "cart/DELETE_FROM_CART"
const DELETE_ALL = "cart/DELETE_ALL"
const CURRENT_ID = "cart/CURRENT_ID"
const ADD_TO_CART = "cart/ADD_TO_CART"

const getCart = (products) => ({ type: GET_CART, payload: products })

export const setCurrent = (id) => ({ type: CURRENT_ID, payload: id })

const deleteFromCart = (id) => ({ type: DELETE_FROM_CART, payload: id })

const deleteAll = () => ({ type: DELETE_ALL })

export const deleteAllProductsFromCart = () => (dispatch) => {
    localStorage.setItem("cart", JSON.stringify([]))
    dispatch(deleteAll())
}

export const deleteProductFromCart = (id) => (dispatch) => {
    const storage = JSON.parse(localStorage.getItem('cart'))
    const filtered = storage.filter(item=>item.id!==id)
    localStorage.setItem("cart", JSON.stringify(filtered))
    dispatch(deleteFromCart(id))
}

export const addProductToCart = (id) => (dispatch) => {
    const storage = JSON.parse(localStorage.getItem('cart'))
    const finded = storage.find(item => item.id === id)
    if (finded) {
        let count = finded.count + 1

        let findedIndex = storage.findIndex(item => item.id === id)
        const newStorage = storage.toSpliced(findedIndex, 1, {
            id: id, count
        })
        localStorage.setItem("cart", JSON.stringify(newStorage))
        dispatch(getCart(newStorage))
        return null
    }
    if (!finded) {
        const newStorage = [...storage, {id: id, count: 1 }]
        localStorage.setItem("cart", JSON.stringify(newStorage))
        dispatch(getCart(newStorage))   
        return null
      }
}

export const getCartFromStorage = () => (dispatch) => {
    const cart = JSON.parse(localStorage.getItem('cart'))
    if (!cart) {
        localStorage.setItem("cart", JSON.stringify([]))
    }
    dispatch(getCart(cart || []));
}

const initialState = {
    cart: {},
    currentId: null
}

const cartReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case GET_CART:
            const newState = payload.reduce((acc, cur) => {
                acc[cur.id] = cur
                return acc
            }, {})
            state.cart = newState
            return state;
        case ADD_TO_CART:
            const stateAddCart = { ...state }
            stateAddCart[payload] ={id: payload, count: 1}
            return stateAddCart;
        case DELETE_FROM_CART:
            const stateForDelete = { ...state }
            delete stateForDelete.cart[payload]
            return stateForDelete;
        case DELETE_ALL:
            return {...state, cart: {}}
        case CURRENT_ID:
            const currentIdState = { ...state }
            currentIdState.currentId = payload
            return currentIdState;
        default:
            return state;
    }
}

export default cartReducer;