import React from 'react'
import ProductCart from '../productCart'
import { useSelector } from 'react-redux'
const productListConfig = {
  fav: {
    cross: false,
    button: true,
  },
  all: {
    cross: false,
    button: true,
  },
  cart: {
    cross: true,
    button: false,
  }
}
export const ProductList = ({productListId}) => {
  const {cross, button} = productListConfig[productListId]
  const products = useSelector(state => Object.values(state.products)) || []
  let productsForRender = null
  if(productListId === "fav"){
    productsForRender=products.filter(item=>item.fav)
  }
  if(productListId === "cart"){
    productsForRender=products.filter(item=>item.cart)
  }
  if(productListId === "all"){
    productsForRender=[...products]
  }
  return (
    <ul>
      {productsForRender.map(product => (
        <ProductCart key={product.id} productId={product.id} cross={cross} button={button} />
      ))}
    </ul>

  )
}
