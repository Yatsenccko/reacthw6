import React from 'react'
import { Formik, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import Button from '../button';
import { StyledForm } from './styled';
import { useDispatch } from 'react-redux';
import { deleteAllProductsFromCart } from '../../store/reducers/cartReducer'
import { removeAllProducts } from '../../store/reducers/productsReducer'
import { useNavigate } from 'react-router-dom'

const orderSchema = Yup.object().shape({
    firstName: Yup.string()
      .min(2, "Too Short!")
      .max(50, "Too Long!")
      .required("Firstname is required"),
  
    lastName: Yup.string()
      .min(2, "Too Short!")
      .max(50, "Too Long!")
      .required("Lastname is required"),

      adress: Yup.string()
      .min(10, "Too Short!")
      .max(50, "Too Long!")
      .required("Adress is required"),

      age: Yup.string()
      .required("Age is required"),
  
    mobile: Yup.string()
      .required("Phone number is required")
  });
const FormCart = () => {
    const dispatch = useDispatch()
    const navigate = useNavigate();

    return (
        <div>
      <h1>Order</h1>
      <Formik
        initialValues={{
          firstName: '',
          lastName: '',
          age: '',
          adress: '',
          mobile: '',
         
        }}
        validationSchema={orderSchema}
        onSubmit={async (values) => {
          await new Promise((r) => setTimeout(r, 500));
          dispatch(deleteAllProductsFromCart())
          dispatch(removeAllProducts())
          navigate("/")
        }}
        
      >
         {(formik) => {
        const { errors, touched, isValid, dirty } = formik;
        return (
        <StyledForm>
          <label htmlFor="firstName">First Name</label>
          <Field id="firstName" name="firstName" placeholder="Jane" className={errors.firstName && touched.firstName ? 
                  "input-error" : null}/>
          <ErrorMessage name="firstName" component="span" className="error" />
  
          <label htmlFor="lastName">Last Name</label>
          <Field id="lastName" name="lastName" placeholder="Doe" className={errors.lastName && touched.lastName ? 
                  "input-error" : null}/>
          <ErrorMessage name="lastName" component="span" className="error" />

          <label htmlFor="age">Age</label>
          <Field id="age" name="age" placeholder="Age" className={errors.age && touched.age ? 
                  "input-error" : null} />
          <ErrorMessage name="age" component="span" className="error" />

          <label htmlFor="adress">Adress</label>
          <Field id="adress" name="adress" placeholder="Adress" className={errors.adress && touched.adress ? 
                  "input-error" : null}/>
          <ErrorMessage name="adress" component="span" className="error" />

          <label htmlFor="mobile">Mobile</label>
          <Field id="mobile" name="mobile" placeholder="+3800500000000" className={errors.mobile && touched.mobile ? 
                  "input-error" : null}/>
          <ErrorMessage name="mobile" component="span" className="error" />
  
          
          <Button type="submit" text="Order" styles="green"/>
        </StyledForm>
        )}}
      </Formik>
    </div>
    )
}

  
  export default FormCart