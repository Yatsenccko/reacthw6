import React from 'react'
import {StyledBtn} from "./styled"
const Button = (props) => {
  const {
    text, onClick, styles, type
} = props
  return (
    <StyledBtn onClick={onClick} $bgStyle = {styles} type={type?type:"button"} >{text}</StyledBtn>
  )
}

export default Button