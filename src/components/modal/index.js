import React from 'react'
import { StyledModal, ModalContent, StyledClose } from "./styled"
import Button from "../button"
import { useDispatch, useSelector } from 'react-redux';
import { toggleModal } from '../../store/reducers/modalReducer';
import { addProductToCart, deleteProductFromCart } from '../../store/reducers/cartReducer';
import { addToCart, removeFromCart } from '../../store/reducers/productsReducer';
const config = {
  modalAddToCart: {
    id: "modalAddToCart",
    title: "Add to cart",
    text: "Do you want add to cart?",
    isCloseBtn: true,
    onCart: addProductToCart,
    onProduct: addToCart

  },
  modalDelete: {
    id: "modalDelete",
    title: "modal delete",
    text: "Do you want delete from cart?",
    isCloseBtn: true,
    onCart: deleteProductFromCart,
    onProduct: removeFromCart
  }
}
const Modal = () => {
  const idModal = useSelector(state => state.modal.id)
  const dispatch = useDispatch()
  const currentId = useSelector(state => state.cart.currentId)
  const { isCloseBtn, title, text, onCart, onProduct } = config[idModal]
  return (
    <StyledModal onClick={() => dispatch(toggleModal(null))} >
      <ModalContent onClick={e => e.stopPropagation()} >
        {isCloseBtn && <StyledClose onClick={() => dispatch(toggleModal(null))} >&times;</StyledClose>}
        <h2>{title}</h2>
        <p>{text}</p>
        <Button text="Submit" styles="yellow" onClick={() => {
          dispatch(toggleModal(null))
          dispatch(onCart(currentId))
          dispatch(onProduct(currentId))
        }} />
        <Button text="Cancel" styles="purple" onClick={() => dispatch(toggleModal(null))} />
      </ModalContent>
    </StyledModal>
  )
}

export default Modal