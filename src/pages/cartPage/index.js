import React from 'react'
import { ProductList } from '../../components/product/productList'
import Button from '../../components/button'
import { useDispatch } from 'react-redux'
import { deleteAllProductsFromCart } from '../../store/reducers/cartReducer'
import { removeAllProducts } from '../../store/reducers/productsReducer'
import FormCart  from '../../components/form'
const CartPage = () => {
  const dispatch=useDispatch()

  return (
    <>
    <Button text="Удалить товары с корзины" onClick={()=>{
      dispatch(deleteAllProductsFromCart())
      dispatch(removeAllProducts())
      }} styles=""/>
    <ProductList productListId="cart"/>
    <FormCart/>
    </>
    
  )
}

export default CartPage